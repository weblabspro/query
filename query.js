/**
 * Query.
 * @author Anton Lyubch <anton.lyubch@gmail.com>
 */

  var query = {};

  query.get = function(val) {
    var result;
    var tmp = [];
    location.search
    .substr(1)
    .split("&")
    .forEach(function (item) {
      tmp = item.split("=");
      if(tmp[0] === val)
        result = decodeURIComponent(tmp[1]);
    });
    return result;
  }

  query.push = function(key,value) {
    data = location.search.slice(1).split('&');

    //unset key from dataString if exists.
    for(var i=0; i<data.length; i++) {
      if(data[i].search(key) !== -1 || data[i] === '')
        data.splice(i,1);
    }

		data.push( key + (value ? '='+value : '') );
    _url = '?' + data.join('&');

    window.history.pushState({},'',_url);
  }
